﻿namespace Lab06 {
    partial class Form1 {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private ComboBox comboBox;
        private ListBox lbHot;
        private ListBox lbCold;
        private Label lblGrad;
        private Label lblTemp;
        private Label lblVlaga;
        private Label lblTlak;
        private Label label5;
        private Label label6;
    }
}