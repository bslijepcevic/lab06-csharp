namespace Lab06 {
    public partial class Form1 : Form {
        public Form1() {
            InitializeComponent();
            vrijeme = new Vrijeme();
            OsvjeziGradove();
            OsvjeziHotGradove();
            OsvjeziColdGradove();
            
        }

        //ova funkcija dodaje u comboBox sve gradove koji se nalaze u funkciji DohvatiGradove() u Vrijeme.cs
        private void OsvjeziGradove() {
            comboBox.Items.Clear();
            foreach (var grad in vrijeme.DohvatiGradove()) {
                comboBox.Items.Add(grad);
            }
        }

        private void OsvjeziHotGradove() {
            lbHot.Items.Clear();
            foreach (var grad in vrijeme.HotGradoviInfo()) {
                lbHot.Items.Add(grad.Description);
            }
        }

        private void OsvjeziColdGradove() {
            lbCold.Items.Clear();
            foreach (var grad in vrijeme.ColdGradoviInfo()) {
                lbCold.Items.Add(grad.Description);
            }
        }

        private void PrikaziPodatkeZaGrad(string grad)
        {
            var podaci = vrijeme.DohvatiPodatkeZaGrad(grad);

            lblGrad.Text = podaci.Grad;
            lblTemp.Text = podaci.Temperatura + " C";
            lblVlaga.Text = podaci.Vlaga + "%";
            lblTlak.Text = podaci.Tlak + " hPa";
        }










        private Vrijeme vrijeme;

        private void InitializeComponent() {
            this.comboBox = new System.Windows.Forms.ComboBox();
            this.lbHot = new System.Windows.Forms.ListBox();
            this.lbCold = new System.Windows.Forms.ListBox();
            this.lblGrad = new System.Windows.Forms.Label();
            this.lblTemp = new System.Windows.Forms.Label();
            this.lblVlaga = new System.Windows.Forms.Label();
            this.lblTlak = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBox
            // 
            this.comboBox.FormattingEnabled = true;
            this.comboBox.Location = new System.Drawing.Point(140, 12);
            this.comboBox.Name = "comboBox";
            this.comboBox.Size = new System.Drawing.Size(121, 28);
            this.comboBox.TabIndex = 0;
            this.comboBox.Text = "Odaberite grad";
            this.comboBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_SelectedIndexChanged);
            // 
            // lbHot
            // 
            this.lbHot.FormattingEnabled = true;
            this.lbHot.ItemHeight = 20;
            this.lbHot.Location = new System.Drawing.Point(31, 345);
            this.lbHot.Name = "lbHot";
            this.lbHot.Size = new System.Drawing.Size(162, 224);
            this.lbHot.TabIndex = 3;
            // 
            // lbCold
            // 
            this.lbCold.FormattingEnabled = true;
            this.lbCold.ItemHeight = 20;
            this.lbCold.Location = new System.Drawing.Point(214, 345);
            this.lbCold.Name = "lbCold";
            this.lbCold.Size = new System.Drawing.Size(171, 224);
            this.lbCold.TabIndex = 4;
            // 
            // lblGrad
            // 
            this.lblGrad.AutoSize = true;
            this.lblGrad.Location = new System.Drawing.Point(153, 78);
            this.lblGrad.Name = "lblGrad";
            this.lblGrad.Size = new System.Drawing.Size(108, 20);
            this.lblGrad.TabIndex = 6;
            this.lblGrad.Text = "(Odaberi grad)";
            // 
            // lblTemp
            // 
            this.lblTemp.AutoSize = true;
            this.lblTemp.Location = new System.Drawing.Point(153, 149);
            this.lblTemp.Name = "lblTemp";
            this.lblTemp.Size = new System.Drawing.Size(108, 20);
            this.lblTemp.TabIndex = 7;
            this.lblTemp.Text = "(Odaberi grad)";
            // 
            // lblVlaga
            // 
            this.lblVlaga.AutoSize = true;
            this.lblVlaga.Location = new System.Drawing.Point(153, 197);
            this.lblVlaga.Name = "lblVlaga";
            this.lblVlaga.Size = new System.Drawing.Size(108, 20);
            this.lblVlaga.TabIndex = 8;
            this.lblVlaga.Text = "(Odaberi grad)";
            // 
            // lblTlak
            // 
            this.lblTlak.AutoSize = true;
            this.lblTlak.Location = new System.Drawing.Point(153, 245);
            this.lblTlak.Name = "lblTlak";
            this.lblTlak.Size = new System.Drawing.Size(108, 20);
            this.lblTlak.TabIndex = 9;
            this.lblTlak.Text = "(Odaberi grad)";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 308);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "Najtopliji gradovi:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(214, 308);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "Najhladniji gradovi:";
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(407, 613);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lblTlak);
            this.Controls.Add(this.lblVlaga);
            this.Controls.Add(this.lblTemp);
            this.Controls.Add(this.lblGrad);
            this.Controls.Add(this.lbCold);
            this.Controls.Add(this.lbHot);
            this.Controls.Add(this.comboBox);
            this.Name = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void comboBox_SelectedIndexChanged(object? sender, EventArgs e)
        {
            PrikaziPodatkeZaGrad(comboBox.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e) {

        }


    }
}