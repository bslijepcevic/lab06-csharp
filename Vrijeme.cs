﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Lab06 {
    class Vrijeme {
        public Vrijeme() {
            vrijeme = new XmlDocument();
            vrijeme.Load("http://vrijeme.hr/hrvatska_n.xml");
        }

        public List<VrijemePodatak> DohvatiPodatke() {
            List<VrijemePodatak> podaci = new List<VrijemePodatak>();

            XmlNodeList gradovi = vrijeme.GetElementsByTagName("Grad");
            foreach (XmlNode grad in gradovi) {
                podaci.Add(new VrijemePodatak(
                    grad["GradIme"].InnerText,
                    grad["Podatci"]["Temp"].InnerText,
                    grad["Podatci"]["Vlaga"].InnerText,
                    grad["Podatci"]["Tlak"].InnerText
                ));
            }
            return podaci;
        }

       
        public IEnumerable<string> DohvatiGradove() {
            var podaci = DohvatiPodatke();

            var gradovi =
                from podatak in podaci
                orderby podatak.Grad
                select podatak.Grad;

            return gradovi;
        }

        
        public VrijemePodatak DohvatiPodatkeZaGrad(string grad) {
            var podaci = DohvatiPodatke();

            var gradovi =
                (from podatak in podaci
                where podatak.Grad == grad
                select podatak).First();

            return gradovi;
        }

        public IEnumerable<VrijemePodatak> HotGradoviInfo() {
            var podaci = DohvatiPodatke();

            var gradovi =
                (from podatak in podaci
                 orderby podatak.Temperatura descending
                 select podatak).Take(5);

            return gradovi;
        }

        public IEnumerable<VrijemePodatak> ColdGradoviInfo() {
            var podaci = DohvatiPodatke();

            var gradovi =
                (from podatak in podaci
                 orderby podatak.Temperatura ascending
                 select podatak).Take(5);

            return gradovi;
        }



        private XmlDocument vrijeme;
    }

}
